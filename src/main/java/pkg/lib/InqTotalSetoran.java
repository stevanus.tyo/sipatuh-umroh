/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.lib;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.log4j.Logger;
import org.json.me.JSONArray;
import org.json.me.JSONObject;

import static pkg.main.SocketServer.token;

/**
 *
 * @author macbook
 */
public class InqTotalSetoran {

	protected int totalppiu;

	private static Logger log = Logger.getLogger(InqTotalSetoran.class);

	public int getTotalSetoranPpiu(String nomor_registrasi) {

		try {

			URL url = new URL(LibConfig.url_SOA + "transaksi/jamaah" + "/" + nomor_registrasi);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("x-access-key", token);
			conn.setConnectTimeout(6000);

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}

			InputStreamReader is = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(is);
			String output;

			while ((output = br.readLine()) != null) {
				JSONObject obj = new JSONObject(output);
				JSONArray arr = obj.getJSONArray("data");
				for (int i = 0; i < arr.length(); i++) {
					totalppiu = arr.getJSONObject(i).getInt("nominal_total");
				}
			}

		} catch (Exception e) {

			totalppiu = 0;

		}

		log.info("Total setoran = " + totalppiu);

		return totalppiu;

	}

}
