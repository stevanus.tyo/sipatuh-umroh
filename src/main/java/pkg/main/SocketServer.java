/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.main;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.SOAPMessage;
import org.apache.log4j.Logger;
import org.json.me.JSONArray;
import org.json.me.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import pkg.controller.ChangePasswordController;
import pkg.controller.InquiryKodeCabangController;
import pkg.controller.InquiryMonitorController;
import pkg.controller.InquiryPeriodeController;
import pkg.controller.InquiryTransaksiDebetController;
import pkg.lib.AsuransiTransfer;
import pkg.lib.FundTransfer;
import pkg.lib.InitConnection;
import pkg.lib.InqTotalSetoran;
import pkg.lib.LibConfig;
import pkg.lib.LibFunction;
import pkg.lib.RequestToken;
import pkg.lib.UmrahSocketException;
import pkg.model.ChangePassword;
import pkg.model.InquiryKodeCabang;
import pkg.model.InquiryMonitor;
import pkg.model.InquiryPeriode;
import pkg.model.InquiryTransaksiDebet;

/**
 *
 * @author macbook
 */
public class SocketServer {

    /* Inisiasi variable global */
    public static String no_reg, kd_cabang, tgl_bayar, no_rek_ppiu, nama_rek_ppiu, no_rek_asuransi, no_rek_dbt,
            nama_rek_asuransi, kd_channel, nomor_ref, nomor_ref_asuransi, timestamp, msgid, message, rc, no_identitas,
            biaya, rencana_berangkat, pendidikan, nama, tempat_lahir, tgl_lahir, ppiu, nmlppiu, pekerjaan, provinsi,
            kabupaten, alamat, asuransi, paket, bank_id, nominal_asuransi, success_indicator, nom_ppiu, no_pasti_umrah,
            nom_total, token, kd_ppiu, biaya_adm, total_setoran, kecamatan, desa, nama_ayah, jenis_identitas,
            kd_asuransi, jenis_kelamin;

    public static Double nominal_ppiu, nominal_total, as;

    private static Logger log = Logger.getLogger(SocketServer.class);

    private static String output_socket;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

        boolean proxyConnection = new InitConnection().getInitializeProxy();

        try (ServerSocket listener = new ServerSocket(LibConfig.port)) {
            log.info("Umroh server is running...");
            ExecutorService pool = Executors.newFixedThreadPool(10000);
            while (true) {
                pool.execute(new SocketUmroh(listener.accept()));
            }
        }

    }

    private static String removeScientificNotation(String value) {
        return new BigDecimal(value).toPlainString();
    }

    private static class SocketUmroh implements Runnable {

        private Socket socket;

        String soapEndpointUrl = LibConfig.url_end_point;
        String soapAction = "";

        LibFunction mysqlq = new LibFunction();
        Statement stmt;
        ResultSet getResultSet;

        SocketUmroh(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            log.info("Client connected " + socket);
            try {
                /* scan request */
                InputStreamReader in = new InputStreamReader(socket.getInputStream());
                BufferedReader bf = new BufferedReader(in);

                /* parse request */
                String request = bf.readLine();
                String parseRequest = request.replaceAll("\\s+", "");
                log.info("Request Message = " + request);

                /* Request inquiry setoran */
                String Request1 = request.substring(87, 117).trim();

                /* Request setoran ppiu */
                String Request2 = request.substring(84, 114).trim();

                /* Request cetak resi */
                String Request3 = request.substring(87, 117).trim();

                /* Request setoran ppiu manual */
                String Request4 = request.substring(84, 114).trim();

                /* Request inquiry berdasarkan nomor registrasi */
                String Request5 = request.substring(87, 117).trim();

                /* Request inquiry setoran asuransi */
                String Request6 = request.substring(87, 117).trim();

                /* Request setoran asuransi */
                String Request7 = request.substring(84, 114).trim();

                /* Request inquiry berdasarkan kode cabang */
                String Request8 = request.substring(87, 117).trim();

                /* Request inquiry berdasarkan periode */
                String Request9 = request.substring(87, 117).trim();

                /* Request ganti password */
                String Request10 = request.substring(87, 117).trim();
                
                /* Request inquiry transaksi debet */
                String Request11 = request.substring(87, 117).trim();
                
                /* Request inquiry monitoring transaksi */
                String Request12 = request.substring(87, 117).trim();

                if (Request1.equals("UMROH Inq SETORAN")) {

                    // ----------------------- cek token first -----------------------------//
                    RequestToken reqToken = new RequestToken();
                    boolean cektoken = reqToken.cekExpiredToken();

                    if (cektoken) {
                        token = reqToken.getToken();
                    } else {
                        token = reqToken.getNewToken();
                        if (token.equals("")) {
                            log.error("Failed create token");
                            throw new Exception("Failed created token");
                        } else {
                            log.info("New token created");
                        }
                    }
                    // ----------------------------------------------------------------------//

                    no_reg = parseRequest.substring(128, 142);

                    try {

                        URL url = new URL(LibConfig.url_SOA + "pembayaran" + "/" + no_reg);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("GET");
                        conn.setRequestProperty("x-access-key", token);
                        conn.setConnectTimeout(6000);

                        if (conn.getResponseCode() != 200) {
                            throw new UmrahSocketException("Nomor registrasi tidak diketemukan");
                        }

                        InputStreamReader is = new InputStreamReader(conn.getInputStream());
                        BufferedReader br = new BufferedReader(is);
                        String output;

                        while ((output = br.readLine()) != null) {
                            output_socket = output;
                            log.info("RESPONSE REST : " + output);
                        }

                        JSONObject obj = new JSONObject(output_socket);
                        JSONObject arr = obj.getJSONObject("data");

                        message = obj.getString("message");
                        rc = obj.getString("RC");

                        for (int i = 0; i < arr.length(); i++) {
                            no_reg = arr.getString("nomor_registrasi");
                            no_identitas = arr.getString("nomor_identitas");
                            biaya = arr.getString("biaya") + ".00";
                            rencana_berangkat = arr.getString("rencana_berangkat");
                            nama = arr.getString("nama");
                            tempat_lahir = arr.getString("tempat_lahir");
                            tgl_lahir = arr.getString("tgl_lahir");
                            ppiu = arr.getString("ppiu");
                            provinsi = arr.getString("provinsi");
                            kabupaten = arr.getString("kabupaten");
                            alamat = arr.getString("alamat");
                            paket = arr.getString("paket");
                            bank_id = arr.getString("bank_id");
                            kecamatan = arr.getString("kecamatan");
                            desa = arr.getString("desa");
                            asuransi = arr.getString("asuransi");
                            jenis_identitas = arr.getString("jenis_identitas");
                            kd_ppiu = arr.getString("kd_ppiu");
                            jenis_kelamin = arr.getString("jenis_kelamin");
                            nama_ayah = arr.getString("nama_ayah");
                        }

                        biaya = biaya.replace(".", "");
                        // nominal_asuransi = nominal_asuransi.replace(".", "");
                        rencana_berangkat = rencana_berangkat.substring(0, 10);
                        rencana_berangkat = rencana_berangkat.replace("-", "");
                        tgl_lahir = tgl_lahir.substring(0, 10);
                        tgl_lahir = tgl_lahir.replace("-", "");
                        biaya_adm = LibConfig.biaya + "00";

                        if (bank_id.equals("null")) {
                            bank_id = "451";
                        } 
                        
                        if (!bank_id.equals("451")) {
                            throw new UmrahSocketException("Nomor registrasi terdaftar di bank lain");
                        }

                        /* Create response socket data */
                        LibFunction func = new LibFunction(); // gunakan function str pad untuk parsing response
                        output_socket = func.str_pad("30001", 5, " ", "STR_PAD_RIGHT")
                                + func.str_pad("0", 8, "0", "STR_PAD_LEFT") + func.str_pad("0", 6, "0", "STR_PAD_LEFT")
                                + func.str_pad(no_reg, 14, " ", "STR_PAD_RIGHT")
                                + func.str_pad(jenis_identitas, 8, " ", "STR_PAD_RIGHT")
                                + func.str_pad(no_identitas, 16, " ", "STR_PAD_RIGHT")
                                + func.str_pad(biaya, 12, "0", "STR_PAD_LEFT")
                                + func.str_pad(rencana_berangkat, 10, " ", "STR_PAD_RIGHT")
                                + func.str_pad(bank_id, 3, "0", "STR_PAD_LEFT")
                                + func.str_pad(nama, 50, " ", "STR_PAD_RIGHT")
                                + func.str_pad(tempat_lahir, 50, " ", "STR_PAD_RIGHT")
                                + func.str_pad(tgl_lahir, 10, " ", "STR_PAD_RIGHT")
                                + func.str_pad(jenis_kelamin, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(kd_ppiu, 4, "0", "STR_PAD_LEFT")
                                + func.str_pad(ppiu, 50, " ", "STR_PAD_RIGHT")
                                + func.str_pad(provinsi, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(kabupaten, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(kecamatan, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(desa, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(alamat, 100, " ", "STR_PAD_RIGHT")
                                // + func.str_pad(kd_asuransi, 4, "0", "STR_PAD_LEFT")
                                // + func.str_pad(asuransi, 50, " ", "STR_PAD_RIGHT")
                                + func.str_pad(paket, 100, " ", "STR_PAD_RIGHT")
                                // + func.str_pad(nominal_asuransi, 12, "0", "STR_PAD_LEFT")
                                + func.str_pad(biaya_adm, 12, "0", "STR_PAD_LEFT")
                                + func.str_pad(nama_ayah, 20, " ", "STR_PAD_RIGHT")
                                + func.str_pad(asuransi, 50, " ", "STR_PAD_RIGHT");

                        conn.disconnect();

                        log.info("OUTPUT SOCKET : " + output_socket);

                    } catch (Exception e) {

                        throw new UmrahSocketException(e.getMessage());

                    }

                }

                if (Request2.equals("UMROH SETORAN AWAL")) {

                    // ----------------------- cek token first -----------------------------//
                    RequestToken reqToken = new RequestToken();
                    boolean cektoken = reqToken.cekExpiredToken();

                    if (cektoken) {
                        token = reqToken.getToken();
                    } else {
                        token = reqToken.getNewToken();
                        if (token.equals("")) {
                            log.error("Failed create token");
                            throw new Exception("Failed created token");
                        } else {
                            log.info("New token created");
                        }
                    }
                    // ----------------------------------------------------------------------//

                    no_rek_dbt = request.substring(142, 153);
                    bank_id = request.substring(158, 161);
                    no_reg = request.substring(200, 214);
                    kd_cabang = request.substring(214, 218);
                    nom_ppiu = request.substring(218, 230);
                    kd_channel = request.substring(262, 266);
                    kd_ppiu = request.substring(200, 204);
                    tgl_bayar = LibFunction.getDatetime("yyyy-MM-dd HH:mm:ss");
                    timestamp = LibFunction.getDatetime("yyyyMMddHHmmssSSS");
                    msgid = no_reg + timestamp;

                    nominal_ppiu = Double.parseDouble(nom_ppiu) / 100;
                    nmlppiu = String.valueOf(nominal_ppiu);
                    nmlppiu = removeScientificNotation(nmlppiu);

                    /* GET NOREK PPIU FROM DB */
                    stmt = mysqlq.getMysqlConn().createStatement();
                    mysqlq.setQuery("SELECT * FROM T_PPIU WHERE ID_PPIU = '" + kd_ppiu + "'");
                    getResultSet = stmt.executeQuery(mysqlq.getQuery());

                    boolean cekfetchppiu = getResultSet.next();
                    if (!cekfetchppiu) {
                        throw new UmrahSocketException("Nomor dan nama rekening ppiu tdk diketemukan di database");
                    } else {
                        no_rek_ppiu = getResultSet.getString("REK_PPIU");
                        nama_rek_ppiu = getResultSet.getString("NAMA_PPIU");
                    }
                    stmt.close();
                    
                    /* Penambahan CR validasi setoran awal */

                    boolean setoranawal;
                    boolean setoranketiga;

                    boolean cekSetoranAwal = new LibFunction().cekSetoranAwal(no_reg);

                    if (cekSetoranAwal) {
                        setoranawal = false;
                    } else {
                        setoranawal = true;
                    }

                    if (setoranawal) {
                        if (nominal_ppiu < 10000000.00) {
                            throw new UmrahSocketException("Setoran awal tdk boleh < 10jt");
                        }
                    } else {
                        if (nominal_ppiu < 500000.00) {
                            throw new UmrahSocketException("Setoran ppiu tdk boleh < 500rb");
                        } else {
                            // cek setoran ketiga
                            setoranketiga = new LibFunction().cekSetoranKetiga(no_reg);
                            if (setoranketiga) {
                                String totalsetoran = Integer.toString(new InqTotalSetoran().getTotalSetoranPpiu(no_reg));
                                double totalpaid = Double.parseDouble(totalsetoran);
                                double mustpaid = 20000000.00 - totalpaid;
                                if (nominal_ppiu < mustpaid) {
                                    throw new UmrahSocketException("Setoran ketiga tdk boleh < " + mustpaid);
                                }
                            }
                        }
                    }


                    /* overbooking ppiu */
                    log.info("Overbooking from " + no_rek_dbt + " to ppiu acc " + no_rek_ppiu);
                    FundTransfer transfer = new FundTransfer();
                    SOAPMessage responsesoap = transfer.callSoapWebService(soapEndpointUrl, soapAction);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    responsesoap.writeTo(baos);
                    String responseStr = new String(baos.toByteArray());

                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse((new InputSource(new StringReader(responseStr))));
                    doc.getDocumentElement().normalize();

                    NodeList twsRC = doc.getElementsByTagName("successIndicator");
                    for (int i = 0; i < twsRC.getLength(); i++) {
                        Node node = twsRC.item(i);
                        success_indicator = node.getTextContent();
                    }

                    if (!success_indicator.equals("Success")) {

//                        /* send response */
//                        PrintWriter pr = new PrintWriter(socket.getOutputStream());
//                        pr.println(output_socket);
//                        pr.flush();
                        NodeList list = doc.getElementsByTagName("transactionId");
                        for (int i = 0; i < list.getLength(); i++) {
                            Node node = list.item(i);
                            nomor_ref = node.getTextContent();
                        }

                        String datetime = LibFunction.getDatetime("yyyy-MM-dd HH:mm:ss");

                        // -------------------- insert log into db error overbook
                        // ---------------------------------------
                        stmt = mysqlq.getMysqlConn().createStatement();
                        mysqlq.setQuery("INSERT INTO T_T24_OVERBOOK (DEBIT_ACC, DEBIT_AMT, CREDIT_ACC, MSG_ID, STATUS, "
                                + "FT, DATETIME, KETERANGAN, NO_REGISTRASI) VALUES " + "('" + no_rek_dbt + "'," + "'"
                                + nominal_ppiu + "'," + "'" + no_rek_ppiu + "'," + "'" + msgid + "'," + "'"
                                + success_indicator + "'," + "'" + nomor_ref + "'," + "'" + datetime + "'," + "'"
                                + "setoran awal umroh noreg " + no_reg + "'," + "'" + no_reg + "')");
                        stmt.executeUpdate(mysqlq.getQuery());
                        log.info(mysqlq.getQuery());
                        stmt.close();
                        // --------------------------------------------------------------------------------------------

                        throw new UmrahSocketException("Transfer gagal cek rekening kembali");
                    }

                    NodeList list = doc.getElementsByTagName("transactionId");
                    for (int i = 0; i < list.getLength(); i++) {
                        Node node = list.item(i);
                        nomor_ref = node.getTextContent();
                    }

                    baos.reset();

                    String datetime = LibFunction.getDatetime("yyyy-MM-dd HH:mm:ss");

                    // -------------------- insert log into db success overbook
                    // --------------------------------------
                    stmt = mysqlq.getMysqlConn().createStatement();
                    mysqlq.setQuery("INSERT INTO T_T24_OVERBOOK (DEBIT_ACC, DEBIT_AMT, CREDIT_ACC, MSG_ID, STATUS, "
                            + "FT, DATETIME, KETERANGAN, NO_REGISTRASI) VALUES " + "('" + no_rek_dbt + "'," + "'"
                            + nominal_ppiu + "'," + "'" + no_rek_ppiu + "'," + "'" + msgid + "'," + "'"
                            + success_indicator + "'," + "'" + nomor_ref + "'," + "'" + datetime + "'," + "'"
                            + "setoran awal umroh noreg " + no_reg + "'," + "'" + no_reg + "')");
                    stmt.executeUpdate(mysqlq.getQuery());
                    log.info(mysqlq.getQuery());
                    stmt.close();

                    stmt = mysqlq.getMysqlConn().createStatement();
                    mysqlq.setQuery(
                            "INSERT INTO T_LOG_BIAYA (debit_acc, debit_amt, kd_cabang, msg_id, status, nomor_referensi, datetime)"
                            + " VALUES (" + "'" + no_rek_dbt + "'," + "'" + LibConfig.biaya + "'," + "'"
                            + kd_cabang + "'," + "'" + msgid + "'," + "'" + success_indicator + "'," + "'"
                            + nomor_ref + "'," + "'" + datetime + "')");
                    stmt.executeUpdate(mysqlq.getQuery());
                    log.info(mysqlq.getQuery());
                    stmt.close();
                    
                    if (setoranawal) {
                        stmt = mysqlq.getMysqlConn().createStatement();
                        mysqlq.setQuery("INSERT INTO t_flag_setoran_awal VALUES ('" + no_reg + "','" + datetime + "')");
                        stmt.executeUpdate(mysqlq.getQuery());
                        log.info(mysqlq.getQuery());
                        stmt.close();
                    }
                    // --------------------------------------------------------------------------------------------

                    JSONObject requestbody = new JSONObject();

                    requestbody.put("nomor_registrasi", no_reg);
                    requestbody.put("kd_cabang", kd_cabang);
                    requestbody.put("tgl_bayar", tgl_bayar);
                    requestbody.put("nominal_ppiu", nominal_ppiu);
                    requestbody.put("nomor_rek_ppiu", no_rek_ppiu);
                    requestbody.put("nama_rek_ppiu", nama_rek_ppiu);
                    requestbody.put("kd_channel", kd_channel);
                    requestbody.put("nomor_referensi", nomor_ref);

                    /* POST Pembayaran PPIU to SISKOHAT */
                    try {

                        URL url = new URL(LibConfig.url_SOA + "pembayaran");
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Content-Type", "application/json");
                        conn.setRequestProperty("x-access-key", token);
                        conn.setConnectTimeout(6000);

                        String urlParameters = requestbody.toString();

                        conn.setDoOutput(true);
                        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
                        wr.writeBytes(urlParameters);
                        wr.flush();
                        wr.close();

                        int responseCode = conn.getResponseCode();

                        if (responseCode == 200) {
                            log.info("Sending 'POST' request to URL : " + url);
                            log.info("Post parameters : " + urlParameters);
                            log.info("Response Code : " + responseCode);

                            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                            String inputLine;
                            StringBuffer response = new StringBuffer();

                            while ((inputLine = br.readLine()) != null) {
                                response.append(inputLine);
                            }
                            br.close();

                            /* print log response rest */
                            log.info("RESPONSE REST : " + response.toString());

                            /* parse json response post to insert log db */
                            String output = response.toString();
                            JSONObject obj = new JSONObject(output);
                            JSONObject arr = obj.getJSONObject("data");

                            String rc = obj.getString("RC");
                            String message = obj.getString("message");

                            if (!rc.equals("00")) {
                                throw new UmrahSocketException(message);
                            }

                            String resbankid = arr.getString("bank_id");
                            String resnoref = arr.getString("referensi");
                            String resnopasti = arr.getString("nomor_pasti_umrah");
                            String restotal = arr.getString("nominal_ppiu");
                            String resnoreg = arr.getString("nomor_registrasi");
                            String restanggalbayar = LibFunction.getDatetime("yyyy-MM-dd HH:mm:ss");

                            stmt = mysqlq.getMysqlConn().createStatement();
                            mysqlq.setQuery("INSERT INTO T_SETORAN (no_registrasi, bank_id," + "no_referensi, "
                                    + "no_pasti_umrah, " + "nominal_total, " + "tgl_bayar) VALUES ('" + resnoreg + "','"
                                    + resbankid + "','" + resnoref + "','" + resnopasti + "','" + restotal + "','"
                                    + restanggalbayar + "')");
                            stmt.executeUpdate(mysqlq.getQuery());
                            stmt.close();

                            /* output socket */
                            LibFunction func = new LibFunction();
                            output_socket = func.str_pad("30001", 5, " ", "STR_PAD_RIGHT")
                                    + func.str_pad("0", 8, "0", "STR_PAD_LEFT") // seqnumber
                                    + func.str_pad("0", 6, "0", "STR_PAD_LEFT") // response code sukses 00
                                    + func.str_pad(no_reg, 14, " ", "STR_PAD_RIGHT")
                                    + func.str_pad(nomor_ref, 12, " ", "STR_PAD_RIGHT");
                            log.info("OUTPUT SOCKET : " + output_socket);

                        } else {
                            LibFunction func = new LibFunction();
                            output_socket = func.str_pad("30001", 5, " ", "STR_PAD_RIGHT")
                                    + func.str_pad("0", 8, "0", "STR_PAD_LEFT") // seqnumber
                                    + func.str_pad("01", 6, "0", "STR_PAD_LEFT") // response code sukses 00
                                    + func.str_pad("TF SUKSES POST GAGAL SILAHKAN LAKUKAN POST MANUAL", 50, " ",
                                            "STR_PAD_RIGHT");
                            log.info("OUTPUT SOCKET : " + output_socket);
                        }
                    } catch (Exception e) {
                        log.error("Exception in NetClientPOST: " + e);
                        LibFunction func = new LibFunction();
                        output_socket = func.str_pad("30001", 5, " ", "STR_PAD_RIGHT")
                                + func.str_pad("0", 8, "0", "STR_PAD_LEFT") // seqnumber
                                + func.str_pad("01", 6, "0", "STR_PAD_LEFT") // response code sukses 00
                                + func.str_pad(e.getMessage(), 50, " ", "STR_PAD_RIGHT");
                        log.info("OUTPUT SOCKET : " + output_socket);
                    }

                }

                if (Request3.equals("Cetak Setoran Awal Umroh")) {

                    // ----------------------- cek token first -----------------------------//
                    RequestToken reqToken = new RequestToken();
                    boolean cektoken = reqToken.cekExpiredToken();

                    if (cektoken) {
                        token = reqToken.getToken();
                    } else {
                        token = reqToken.getNewToken();
                        if (token.equals("")) {
                            log.error("Failed create token");
                            throw new Exception("Failed created token");
                        } else {
                            log.info("New token created");
                        }
                    }
                    // ----------------------------------------------------------------------//

                    no_reg = parseRequest.substring(104, 118);

                    try {

                        nomor_ref = parseRequest.substring(118, 130);

                    } catch (Exception e) {

                        throw new UmrahSocketException("Nomor FT salah");

                    }

                    try {

                        URL url = new URL(LibConfig.url_SOA + "cetak" + "/" + no_reg + "/" + nomor_ref);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("GET");
                        conn.setRequestProperty("x-access-key", token);
                        conn.setConnectTimeout(6000);

                        if (conn.getResponseCode() != 200) {
                            throw new RuntimeException("Cek nomor FT dan nomor registrasi");
                        }

                        InputStreamReader is = new InputStreamReader(conn.getInputStream());
                        BufferedReader br = new BufferedReader(is);
                        String output;

                        while ((output = br.readLine()) != null) {
                            output_socket = output;
                            log.info("Response Rest : " + output);
                        }

                        JSONObject obj = new JSONObject(output_socket);
                        JSONArray arr = obj.getJSONArray("data");

                        message = obj.getString("message");
                        rc = obj.getString("RC");

                        for (int i = 0; i < arr.length(); i++) {
                            no_reg = arr.getJSONObject(i).getString("nomor_registrasi");
                            no_pasti_umrah = arr.getJSONObject(i).getString("nomor_pasti_umrah");
                            no_identitas = arr.getJSONObject(i).getString("nomor_identitas");
                            biaya = arr.getJSONObject(i).getString("biaya");
                            rencana_berangkat = arr.getJSONObject(i).getString("rencana_berangkat");
                            pendidikan = arr.getJSONObject(i).getString("pendidikan");
                            bank_id = arr.getJSONObject(i).getString("bank_id");
                            nama = arr.getJSONObject(i).getString("nama");
                            tempat_lahir = arr.getJSONObject(i).getString("tempat_lahir");
                            tgl_lahir = arr.getJSONObject(i).getString("tgl_lahir");
                            ppiu = arr.getJSONObject(i).getString("ppiu");
                            jenis_kelamin = arr.getJSONObject(i).getString("jenis_kelamin");
                            pekerjaan = arr.getJSONObject(i).getString("pekerjaan");
                            provinsi = arr.getJSONObject(i).getString("provinsi");
                            kabupaten = arr.getJSONObject(i).getString("kabupaten");
                            kecamatan = arr.getJSONObject(i).getString("kecamatan");
                            desa = arr.getJSONObject(i).getString("desa");
                            alamat = arr.getJSONObject(i).getString("alamat");
                            asuransi = arr.getJSONObject(i).getString("asuransi");
                            paket = arr.getJSONObject(i).getString("paket");
                            nominal_asuransi = arr.getJSONObject(i).getString("nominal_asuransi");
                            nom_ppiu = arr.getJSONObject(i).getString("nominal_ppiu");
                            nom_total = arr.getJSONObject(i).getString("nominal_total");
                            nomor_ref = arr.getJSONObject(i).getString("nomor_referensi");
                            tgl_bayar = arr.getJSONObject(i).getString("tgl_bayar");
                            nama_ayah = arr.getJSONObject(i).getString("nama_ayah");
                        }

                        biaya = biaya.replace(".", "") + "00";

                        if (nominal_asuransi.equals("null")) {

                            /* CEK ASURANSI YG SUDAH DISETOR */
                            stmt = mysqlq.getMysqlConn().createStatement();
                            mysqlq.setQuery("SELECT * FROM T_SETORAN_ASURANSI WHERE no_registrasi = '" + no_reg + "'");
                            getResultSet = stmt.executeQuery(mysqlq.getQuery());
                            boolean cekfetchass = getResultSet.next();

                            if (cekfetchass) {
                                nominal_asuransi = getResultSet.getString("nominal_asuransi");
                            } else {
                                nominal_asuransi = "0";
                            }
                        }

                        if (nom_ppiu.equals("null")) {

                            nom_ppiu = "0";

                        }

                        nominal_asuransi = nominal_asuransi + "00";

                        nom_ppiu = nom_ppiu + "00";
                        nom_total = nom_total.replace(".", "") + "00";
                        rencana_berangkat = rencana_berangkat.substring(0, 10);
                        rencana_berangkat = rencana_berangkat.replace("-", "");
                        tgl_lahir = tgl_lahir.substring(0, 10);
                        tgl_lahir = tgl_lahir.replace("-", "");

                        String tgl_byr = tgl_bayar.substring(0, 10);
                        String jam_byr = tgl_bayar.substring(11, 19);
                        tgl_bayar = tgl_byr + " " + jam_byr;
                        tgl_bayar = LibFunction.setTimezone("yyyy-MM-dd HH:mm:ss", LibConfig.timezone, tgl_bayar);
                        tgl_bayar = tgl_bayar.replace("-", "");
                        tgl_bayar = tgl_bayar.replace(":", "");
                        tgl_bayar = tgl_bayar.replaceAll("\\s+", "");

                        InqTotalSetoran ts = new InqTotalSetoran();
                        total_setoran = Integer.toString(ts.getTotalSetoranPpiu(no_reg)) + "00";

                        /* Create response socket data */
                        LibFunction func = new LibFunction(); // gunakan function str pad untuk parsing response
                        output_socket = func.str_pad("30001", 5, " ", "STR_PAD_RIGHT")
                                + func.str_pad("0", 8, "0", "STR_PAD_LEFT") // seqnumber
                                + func.str_pad("0", 6, "0", "STR_PAD_LEFT") // response code sukses 00
                                + func.str_pad(no_reg, 14, " ", "STR_PAD_RIGHT")
                                + func.str_pad(no_pasti_umrah, 7, " ", "STR_PAD_RIGHT")
                                + func.str_pad(no_identitas, 16, " ", "STR_PAD_RIGHT")
                                + func.str_pad(biaya, 12, "0", "STR_PAD_LEFT")
                                + func.str_pad(rencana_berangkat, 10, " ", "STR_PAD_RIGHT")
                                + func.str_pad(pendidikan, 10, " ", "STR_PAD_RIGHT")
                                + func.str_pad(bank_id, 3, "0", "STR_PAD_LEFT")
                                + func.str_pad(nama, 50, " ", "STR_PAD_RIGHT")
                                + func.str_pad(tempat_lahir, 50, " ", "STR_PAD_RIGHT")
                                + func.str_pad(tgl_lahir, 10, " ", "STR_PAD_RIGHT")
                                + func.str_pad(ppiu, 50, " ", "STR_PAD_RIGHT")
                                + func.str_pad(pekerjaan, 15, " ", "STR_PAD_RIGHT")
                                + func.str_pad(provinsi, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(kabupaten, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(kecamatan, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(desa, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(alamat, 100, " ", "STR_PAD_RIGHT")
                                + func.str_pad(asuransi, 50, " ", "STR_PAD_RIGHT")
                                + func.str_pad(paket, 100, " ", "STR_PAD_RIGHT")
                                + func.str_pad(nominal_asuransi, 12, "0", "STR_PAD_LEFT")
                                + func.str_pad(nom_ppiu, 12, "0", "STR_PAD_LEFT")
                                + func.str_pad(nom_total, 12, "0", "STR_PAD_LEFT")
                                + func.str_pad(tgl_bayar, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(nomor_ref, 12, " ", "STR_PAD_RIGHT")
                                + func.str_pad(total_setoran, 12, "0", "STR_PAD_LEFT")
                                + func.str_pad(jenis_kelamin, 10, " ", "STR_PAD_RIGHT")
                                + func.str_pad(nama_ayah, 20, " ", "STR_PAD_RIGHT");

                        conn.disconnect();

                        log.info("OUTPUT SOCKET : " + output_socket);

                    } catch (Exception e) {

                        throw new UmrahSocketException(e.getMessage());

                    }
                }

                if (Request4.equals("UMROH SETORAN AWAL MANUAL")) {

                    // ----------------------- cek token first -----------------------------//
                    RequestToken reqToken = new RequestToken();
                    boolean cektoken = reqToken.cekExpiredToken();

                    if (cektoken) {
                        token = reqToken.getToken();
                    } else {
                        token = reqToken.getNewToken();
                        if (token.equals("")) {
                            log.error("Failed create token");
                            throw new Exception("Failed created token");
                        } else {
                            log.info("New token created");
                        }
                    }
                    // ----------------------------------------------------------------------//

                    no_rek_dbt = request.substring(142, 153).trim();
                    bank_id = request.substring(158, 161).trim();
                    no_reg = request.substring(200, 214).trim();
                    kd_cabang = request.substring(214, 218).trim();
                    nom_ppiu = request.substring(218, 230).trim();
                    nominal_asuransi = request.substring(230, 242).trim();
                    kd_channel = request.substring(262, 266).trim();
                    kd_ppiu = request.substring(200, 204).trim();
                    nama_rek_asuransi = request.substring(276, 326).trim();
                    nomor_ref = request.substring(326, 338).trim();

                    /* GET NOREK PPIU FROM DB */
                    stmt = mysqlq.getMysqlConn().createStatement();
                    mysqlq.setQuery("SELECT * FROM T_PPIU WHERE ID_PPIU = '" + kd_ppiu + "'");
                    getResultSet = stmt.executeQuery(mysqlq.getQuery());

                    boolean cekfetchppiu = getResultSet.next();
                    if (!cekfetchppiu) {
                        throw new UmrahSocketException("Nomor dan nama rekening ppiu tdk diketemukan di database");
                    } else {
                        no_rek_ppiu = getResultSet.getString("REK_PPIU");
                        nama_rek_ppiu = getResultSet.getString("NAMA_PPIU");
                    }
                    stmt.close();

                    /* GET NOREK ASURANSI FROM DB */
                    stmt = mysqlq.getMysqlConn().createStatement();
                    mysqlq.setQuery("SELECT * FROM T_ASURANSI WHERE NAMA_ASURANSI = '" + nama_rek_asuransi + "'");
                    getResultSet = stmt.executeQuery(mysqlq.getQuery());

                    boolean cekfetchass = getResultSet.next();
                    if (!cekfetchass) {
                        throw new UmrahSocketException("Nomor dan nama rekening asuransi tdk diketemukan di db");
                    } else {
                        no_rek_asuransi = getResultSet.getString("REK_ASURANSI");
                    }
                    stmt.close();

                    tgl_bayar = LibFunction.getDatetime("yyyy-MM-dd HH:mm:ss");
                    timestamp = LibFunction.getDatetime("yyyyMMddHHmmssSSS");
                    msgid = no_reg + timestamp;

                    as = Double.parseDouble(nominal_asuransi) / 100;
                    nominal_ppiu = Double.parseDouble(nom_ppiu) / 100;
                    nominal_total = as + nominal_ppiu;

                    if (nominal_ppiu < 500000.00) {
                        throw new UmrahSocketException("Nominal ppiu tdk blh kurang dari 500rb");
                    }

                    /* Cek duplicate POST to kemenag */
                    boolean cekduplicateflag;

                    try {

                        URL url = new URL(LibConfig.url_SOA + "cetak" + "/" + no_reg + "/" + nomor_ref);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("GET");
                        conn.setRequestProperty("x-access-key", token);
                        conn.setConnectTimeout(6000);

                        if (conn.getResponseCode() != 200) {
                            cekduplicateflag = true;
                        } else {
                            cekduplicateflag = false;
                        }

                        conn.disconnect();

                    } catch (Exception e) {
                        throw new UmrahSocketException("Error cek duplicate POST");
                    }

                    if (!cekduplicateflag) {
                        throw new UmrahSocketException("FT already posted");
                    }

                    /* Initialize request body */
                    JSONObject requestbody = new JSONObject();
                    requestbody.put("nomor_registrasi", no_reg);
                    requestbody.put("kd_cabang", kd_cabang);
                    requestbody.put("tgl_bayar", tgl_bayar);
                    requestbody.put("nominal_ppiu", nominal_ppiu);
                    requestbody.put("nominal_asuransi", as);
                    requestbody.put("nomor_rek_ppiu", no_rek_ppiu);
                    requestbody.put("nama_rek_ppiu", nama_rek_ppiu);
                    requestbody.put("nomor_rek_asuransi", no_rek_asuransi);
                    requestbody.put("nama_rek_asuransi", nama_rek_asuransi);
                    requestbody.put("kd_channel", kd_channel);
                    requestbody.put("nominal_total", nominal_total);
                    requestbody.put("nomor_referensi", nomor_ref);

                    try {

                        URL url = new URL(LibConfig.url_SOA + "pembayaran");
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Content-Type", "application/json");
                        conn.setRequestProperty("x-access-key", token);
                        conn.setConnectTimeout(6000);

                        String urlParameters = requestbody.toString();

                        conn.setDoOutput(true);
                        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
                        wr.writeBytes(urlParameters);
                        wr.flush();
                        wr.close();

                        int responseCode = conn.getResponseCode();

                        if (responseCode == 200) {
                            log.info("Sending 'POST' request to URL : " + url);
                            log.info("Post parameters : " + urlParameters);
                            log.info("Response Code : " + responseCode);

                            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                            String inputLine;
                            StringBuffer response = new StringBuffer();

                            while ((inputLine = br.readLine()) != null) {
                                response.append(inputLine);
                            }
                            br.close();

                            /* print log response rest */
                            log.info("RESPONSE REST : " + response.toString());

                            /* parse json response post to insert log db */
                            String output = response.toString();
                            JSONObject obj = new JSONObject(output);
                            JSONObject arr = obj.getJSONObject("data");
                            if (!nominal_asuransi.equals("000000000000")) {
                                for (int i = 0; i < arr.length(); i++) {

                                    String resnopasti = arr.getString("nomor_pasti_umrah");

                                    stmt = mysqlq.getMysqlConn().createStatement();
                                    mysqlq.setQuery("INSERT INTO T_SETORAN_ASURANSI (no_registrasi, bank_id,"
                                            + "no_referensi, " + "no_pasti_umrah, " + "nominal_asuransi, "
                                            + "kd_cabang, " + "tgl_bayar) VALUES ('" + no_reg + "','" + "451" + "','"
                                            + nomor_ref + "','" + resnopasti + "','" + as + "','" + kd_cabang + "','"
                                            + tgl_bayar + "')");
                                    stmt.executeUpdate(mysqlq.getQuery());
                                    stmt.close();
                                }
                            } else {

                                stmt = mysqlq.getMysqlConn().createStatement();
                                mysqlq.setQuery("INSERT INTO T_SETORAN (no_registrasi, bank_id," + "no_referensi, "
                                        + "kd_cabang, " + "nominal_total, " + "tgl_bayar) VALUES ('" + no_reg + "','"
                                        + "451" + "','" + nomor_ref + "','" + kd_cabang + "','" + nominal_ppiu + "','"
                                        + tgl_bayar + "')");
                                stmt.executeUpdate(mysqlq.getQuery());
                                stmt.close();

                            }

                            /* output socket */
                            LibFunction func = new LibFunction();
                            output_socket = func.str_pad("30001", 5, " ", "STR_PAD_RIGHT")
                                    + func.str_pad("0", 8, "0", "STR_PAD_LEFT") // seqnumber
                                    + func.str_pad("0", 6, "0", "STR_PAD_LEFT") // response code sukses 00
                                    + func.str_pad(no_reg, 14, " ", "STR_PAD_RIGHT")
                                    + func.str_pad(nomor_ref, 12, " ", "STR_PAD_RIGHT");
                            log.info("OUTPUT SOCKET : " + output_socket);

                        } else {
                            throw new UmrahSocketException("POST MANUAL GAGAL SILAHKAN LAKUKAN COBA KEMBALI");
                        }

                    } catch (Exception e) {
                        throw new UmrahSocketException(e.getMessage());
                    }
                }

                if (Request5.equals("Umroh Inq Transaksi")) {

                    // ----------------------- cek token first -----------------------------//
                    RequestToken reqToken = new RequestToken();
                    boolean cektoken = reqToken.cekExpiredToken();

                    if (cektoken) {
                        token = reqToken.getToken();
                    } else {
                        token = reqToken.getNewToken();
                        if (token.equals("")) {
                            log.error("Failed create token");
                            throw new Exception("Failed created token");
                        } else {
                            log.info("New token created");
                        }
                    }
                    // ----------------------------------------------------------------------//

                    no_reg = request.substring(187, 201).trim();

                    try {

                        URL url = new URL(LibConfig.url_SOA + "transaksi/jamaah" + "/" + no_reg);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("GET");
                        conn.setRequestProperty("x-access-key", token);
                        conn.setConnectTimeout(6000);
                        int rescode = conn.getResponseCode();
                        rescode = 200;

                        if (rescode != 200) {
                            throw new RuntimeException("Nomor Registrasi Tidak Ditemukan");
                        }
                        InputStreamReader is = new InputStreamReader(conn.getInputStream());
                        BufferedReader br = new BufferedReader(is);
                        String output;

                        while ((output = br.readLine()) != null) {

                            JSONObject obj = new JSONObject(output);
                            String rc = obj.getString("RC");
                            String message = obj.getString("message");
                            JSONArray data = obj.getJSONArray("data");
                            JSONArray arr = new JSONArray();

                            for (int i = 0; i < data.length(); i++) {

                                String noreg = data.getJSONObject(i).getString("nomor_registrasi");
                                String nopasti = data.getJSONObject(i).getString("nomor_pasti_umrah");
                                String bankid = data.getJSONObject(i).getString("bank_id");
                                String nama = data.getJSONObject(i).getString("nama");
                                String ppiu = data.getJSONObject(i).getString("ppiu");
                                String asuransi = data.getJSONObject(i).getString("asuransi");
                                String paket = data.getJSONObject(i).getString("paket");
                                String nomass = data.getJSONObject(i).getString("nominal_asuransi");
                                String nomppiu = data.getJSONObject(i).getString("nominal_ppiu");
                                String nomtot = data.getJSONObject(i).getString("nominal_total");
                                String kdcabang = data.getJSONObject(i).getString("kd_cabang");
                                String noref = data.getJSONObject(i).getString("nomor_referensi");
                                String tglbyr = data.getJSONObject(i).getString("tgl_bayar");

                                JSONArray arr2 = new JSONArray();
                                arr2.put(noreg);
                                arr2.put(nopasti);
                                arr2.put(bankid);
                                arr2.put(nama);
                                arr2.put(ppiu);
                                arr2.put(asuransi);
                                arr2.put(paket);
                                arr2.put(nomass);
                                arr2.put(nomppiu);
                                arr2.put(nomtot);
                                arr2.put(kdcabang);
                                arr2.put(noref);
                                arr2.put(tglbyr);
                                arr.put(arr2);
                            }

                            JSONObject resobj = new JSONObject();
                            resobj.put("RC", rc);
                            resobj.put("message", message);
                            resobj.put("data", arr);

                            output_socket = resobj.toString();
                            log.info("OUTPUT SOCKET : " + output_socket);

                        }

                    } catch (Exception e) {

                        throw new UmrahSocketException(e.getMessage());

                    }
                }

                if (Request6.equals("UMROH Inq SETORAN ASURANSI")) {

                    // ----------------------- cek token first -----------------------------//
                    RequestToken reqToken = new RequestToken();
                    boolean cektoken = reqToken.cekExpiredToken();

                    if (cektoken) {
                        token = reqToken.getToken();
                    } else {
                        token = reqToken.getNewToken();
                        if (token.equals("")) {
                            log.error("Failed create token");
                            throw new Exception("Failed created token");
                        } else {
                            log.info("New token created");
                        }
                    }
                    // ----------------------------------------------------------------------//

                    no_reg = parseRequest.substring(136, 150);
                    log.info("NOMOR REG = " + no_reg);

                    /* Cek total setoran masuk jamaah */
 /*
					 * int tot_setoran = mysqlq.getTotalSetoran(no_reg);
					 * 
					 * if (tot_setoran < 20000000) { throw new
					 * UmrahSocketException("Jumlah setoran kurang dari 20jt"); } else {
					 * nominal_asuransi = "50000.00"; }
                     */
                    InqTotalSetoran ts = new InqTotalSetoran();
                    int tot_setoran = ts.getTotalSetoranPpiu(no_reg);

                    if (tot_setoran < 20000000) {
                        throw new UmrahSocketException("Jumlah setoran kurang dari 20jt");
                    } else {
                        nominal_asuransi = "50000.00";
                    }

                    try {

                        URL url = new URL(LibConfig.url_SOA + "pembayaran" + "/" + no_reg);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("GET");
                        conn.setRequestProperty("x-access-key", token);
                        conn.setConnectTimeout(6000);

                        if (conn.getResponseCode() != 200) {
                            throw new RuntimeException("Failed HTTP Error code " + conn.getResponseCode());
                        }

                        InputStreamReader is = new InputStreamReader(conn.getInputStream());
                        BufferedReader br = new BufferedReader(is);
                        String output;

                        while ((output = br.readLine()) != null) {
                            output_socket = output;
                            log.info("RESPONSE REST : " + output);
                        }

                        JSONObject obj = new JSONObject(output_socket);
                        JSONObject arr = obj.getJSONObject("data");

                        message = obj.getString("message");
                        rc = obj.getString("RC");

                        for (int i = 0; i < arr.length(); i++) {
                            no_reg = arr.getString("nomor_registrasi");
                            no_identitas = arr.getString("nomor_identitas");
                            nama = arr.getString("nama");
                            tempat_lahir = arr.getString("tempat_lahir");
                            tgl_lahir = arr.getString("tgl_lahir");
                            provinsi = arr.getString("provinsi");
                            kabupaten = arr.getString("kabupaten");
                            alamat = arr.getString("alamat");
                            asuransi = arr.getString("asuransi");
                            bank_id = arr.getString("bank_id");
                            kecamatan = arr.getString("kecamatan");
                            desa = arr.getString("desa");
                            jenis_identitas = arr.getString("jenis_identitas");
                            kd_asuransi = arr.getString("kd_asuransi");
                            jenis_kelamin = arr.getString("jenis_kelamin");
                        }

                        nominal_asuransi = nominal_asuransi.replace(".", "");
                        tgl_lahir = tgl_lahir.substring(0, 10);
                        tgl_lahir = tgl_lahir.replace("-", "");

                        if (bank_id.equals("null")) {
                            bank_id = "000";
                        }

                        /* Create response socket data */
                        LibFunction func = new LibFunction(); // gunakan function str pad untuk parsing response
                        output_socket = func.str_pad("30001", 5, " ", "STR_PAD_RIGHT")
                                + func.str_pad("0", 8, "0", "STR_PAD_LEFT") // seqnumber
                                + func.str_pad("0", 6, "0", "STR_PAD_LEFT") // response code sukses 00
                                + func.str_pad(no_reg, 14, " ", "STR_PAD_RIGHT")
                                + func.str_pad(jenis_identitas, 8, " ", "STR_PAD_RIGHT")
                                + func.str_pad(no_identitas, 16, " ", "STR_PAD_RIGHT")
                                + func.str_pad(bank_id, 3, "0", "STR_PAD_LEFT")
                                + func.str_pad(nama, 50, " ", "STR_PAD_RIGHT")
                                + func.str_pad(tempat_lahir, 50, " ", "STR_PAD_RIGHT")
                                + func.str_pad(tgl_lahir, 10, " ", "STR_PAD_RIGHT")
                                + func.str_pad(jenis_kelamin, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(provinsi, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(kabupaten, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(kecamatan, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(desa, 25, " ", "STR_PAD_RIGHT")
                                + func.str_pad(alamat, 100, " ", "STR_PAD_RIGHT")
                                + func.str_pad(kd_asuransi, 4, "0", "STR_PAD_LEFT")
                                + func.str_pad(asuransi, 50, " ", "STR_PAD_RIGHT")
                                + func.str_pad(nominal_asuransi, 12, "0", "STR_PAD_LEFT");

                        conn.disconnect();

                        log.info("OUTPUT SOCKET : " + output_socket);

                    } catch (Exception e) {

                        throw new UmrahSocketException(e.getMessage());

                    }

                }

                if (Request7.equals("UMROH SETORAN ASURANSI")) {

                    // ----------------------- cek token first -----------------------------//
                    RequestToken reqToken = new RequestToken();
                    boolean cektoken = reqToken.cekExpiredToken();

                    if (cektoken) {
                        token = reqToken.getToken();
                    } else {
                        token = reqToken.getNewToken();
                        if (token.equals("")) {
                            log.error("Failed create token");
                            throw new Exception("Failed created token");
                        } else {
                            log.info("New token created");
                        }
                    }
                    // ----------------------------------------------------------------------//

                    /* Parsing request socket */
                    no_rek_dbt = request.substring(142, 153);
                    bank_id = request.substring(158, 161);
                    no_reg = request.substring(200, 214);
                    kd_cabang = request.substring(214, 218);
                    nominal_asuransi = request.substring(234, 246);
                    kd_channel = request.substring(266, 270);
                    kd_asuransi = request.substring(218, 222);

                    as = Double.parseDouble(nominal_asuransi) / 100;

                    /* CEK SETORAN ASURANSI SUDAH DIBAYAR */
                    stmt = mysqlq.getMysqlConn().createStatement();
                    mysqlq.setQuery("SELECT * FROM T_SETORAN_ASURANSI WHERE no_registrasi = '" + no_reg + "'");
                    getResultSet = stmt.executeQuery(mysqlq.getQuery());
                    boolean checkpaid = getResultSet.next();
                    if (checkpaid) {
                        throw new UmrahSocketException("Asuransi sudah dibayar");
                    }

                    /* GET NOREK ASURANSI FROM DB */
                    stmt = mysqlq.getMysqlConn().createStatement();
                    mysqlq.setQuery("SELECT * FROM T_ASURANSI WHERE ID_ASURANSI = '" + kd_asuransi + "'");
                    getResultSet = stmt.executeQuery(mysqlq.getQuery());

                    boolean cekfetchass = getResultSet.next();
                    if (!cekfetchass) {
                        throw new UmrahSocketException("Nomor dan nama rekening asuransi tdk diketemukan di db");
                    } else {
                        no_rek_asuransi = getResultSet.getString("REK_ASURANSI");
                        nama_rek_asuransi = getResultSet.getString("NAMA_ASURANSI");
                    }
                    stmt.close();

                    String datetime = LibFunction.getDatetime("yyyy-MM-dd HH:mm:ss");

                    /* overbooking asuransi */
                    log.info("Overbooking from " + no_rek_dbt + " to asuransi acc " + no_rek_asuransi);
                    timestamp = LibFunction.getDatetime("yyyyMMddHHmmssSSS");
                    msgid = no_reg + timestamp;

                    AsuransiTransfer at = new AsuransiTransfer();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    SOAPMessage responseass = at.callSoapWebService(soapEndpointUrl, soapAction);
                    responseass.writeTo(baos);
                    String response_asuransi = new String(baos.toByteArray());

                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse((new InputSource(new StringReader(response_asuransi))));
                    doc.getDocumentElement().normalize();

                    doc = dBuilder.parse((new InputSource(new StringReader(response_asuransi))));
                    doc.getDocumentElement().normalize();

                    NodeList twsRCAss = doc.getElementsByTagName("successIndicator");
                    for (int i = 0; i < twsRCAss.getLength(); i++) {
                        Node node = twsRCAss.item(i);
                        success_indicator = node.getTextContent();
                    }

                    if (!success_indicator.equals("Success")) {

//                        /* send response */
//                        PrintWriter pr = new PrintWriter(socket.getOutputStream());
//                        pr.println(output_socket);
//                        pr.flush();
                        NodeList list_ass = doc.getElementsByTagName("transactionId");
                        for (int i = 0; i < list_ass.getLength(); i++) {
                            Node node = list_ass.item(i);
                            nomor_ref_asuransi = node.getTextContent();
                        }

                        datetime = LibFunction.getDatetime("yyyy-MM-dd HH:mm:ss");

                        // -------------------- insert log into db error overbook
                        // ---------------------------------------
                        stmt = mysqlq.getMysqlConn().createStatement();
                        mysqlq.setQuery("INSERT INTO T_T24_OVERBOOK (DEBIT_ACC, DEBIT_AMT, CREDIT_ACC, MSG_ID, STATUS, "
                                + "FT, DATETIME, KETERANGAN, NO_REGISTRASI) VALUES " + "('" + no_rek_dbt + "'," + "'"
                                + as + "'," + "'" + no_rek_asuransi + "'," + "'" + msgid + "'," + "'"
                                + success_indicator + "'," + "'" + nomor_ref_asuransi + "'," + "'" + datetime + "',"
                                + "'" + "setoran asuransi umroh noreg " + no_reg + "'," + "'" + no_reg + "')");
                        stmt.executeUpdate(mysqlq.getQuery());
                        log.info(mysqlq.getQuery());
                        stmt.close();
                        // --------------------------------------------------------------------------------------------

                        throw new UmrahSocketException("Transfer gagal cek rekening kembali");
                    }

                    NodeList list_ass = doc.getElementsByTagName("transactionId");
                    for (int i = 0; i < list_ass.getLength(); i++) {
                        Node node = list_ass.item(i);
                        nomor_ref_asuransi = node.getTextContent();
                    }

                    datetime = LibFunction.getDatetime("yyyy-MM-dd HH:mm:ss");

                    // -------------------- insert log into db success overbook
                    // --------------------------------------
                    stmt = mysqlq.getMysqlConn().createStatement();
                    mysqlq.setQuery("INSERT INTO T_T24_OVERBOOK (DEBIT_ACC, DEBIT_AMT, CREDIT_ACC, MSG_ID, STATUS, "
                            + "FT, DATETIME, KETERANGAN, NO_REGISTRASI) VALUES " + "('" + no_rek_dbt + "'," + "'" + as
                            + "'," + "'" + no_rek_asuransi + "'," + "'" + msgid + "'," + "'" + success_indicator + "',"
                            + "'" + nomor_ref_asuransi + "'," + "'" + datetime + "'," + "'"
                            + "setoran asuransi umroh noreg " + no_reg + "'," + "'" + no_reg + "')");
                    stmt.executeUpdate(mysqlq.getQuery());
                    log.info(mysqlq.getQuery());
                    stmt.close();
                    // --------------------------------------------------------------------------------------------

                    /* Send socket response */
                    LibFunction func = new LibFunction(); // gunakan function str pad untuk parsing response
                    output_socket = func.str_pad("30001", 5, " ", "STR_PAD_RIGHT")
                            + func.str_pad("0", 8, "0", "STR_PAD_LEFT") // seqnumber
                            + func.str_pad("0", 6, "0", "STR_PAD_LEFT") // response code sukses 00
                            + func.str_pad(no_reg, 14, " ", "STR_PAD_RIGHT")
                            + func.str_pad(nomor_ref_asuransi, 12, " ", "STR_PAD_RIGHT");
                    log.info("OUTPUT SOCKET: " + output_socket);

                    /* Post Pembayaran Asuransi to Siskohat */
                    JSONObject reqBody = new JSONObject();
                    reqBody.put("nomor_registrasi", no_reg);
                    reqBody.put("kd_cabang", kd_cabang);
                    reqBody.put("tgl_bayar", datetime);
                    reqBody.put("nominal_asuransi", as.toString());
                    reqBody.put("nomor_rek_asuransi", no_rek_asuransi);
                    reqBody.put("nama_rek_asuransi", nama_rek_asuransi);
                    reqBody.put("kd_channel", kd_channel);
                    reqBody.put("nomor_referensi", nomor_ref_asuransi);

                    try {

                        URL url = new URL(LibConfig.url_SOA + "pembayaran/asuransi");
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Content-Type", "application/json");
                        conn.setRequestProperty("x-access-key", token);
                        conn.setConnectTimeout(6000);

                        String urlParameters = reqBody.toString();

                        conn.setDoOutput(true);
                        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
                        wr.writeBytes(urlParameters);
                        wr.flush();
                        wr.close();

                        int responseCode = conn.getResponseCode();

                        log.info("Sending 'POST' request to URL : " + url);
                        log.info("Post parameters : " + urlParameters);
                        log.info("Response Code : " + responseCode);

                        if (responseCode == 200) {

                            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                            String inputLine;
                            StringBuffer response = new StringBuffer();

                            while ((inputLine = br.readLine()) != null) {
                                response.append(inputLine);
                            }
                            br.close();

                            /* print log response rest */
                            log.info("RESPONSE REST : " + response.toString());

                            /* parse json response post to insert log db */
                            String output = response.toString();
                            JSONObject obj = new JSONObject(output);
                            JSONObject data = obj.getJSONObject("data");

                            String rc = obj.getString("RC");

                            if (rc.equals("00")) {

                                String noreg = data.getString("nomor_registrasi");
                                String nopasti = data.getString("nomor_pasti_umrah");
                                String kdcabang = data.getString("kd_cabang");
                                String bankid = data.getString("bank_id");
                                String tglbayar = LibFunction.getDatetime("yyyy-MM-dd HH:mm:ss");
                                String nom_asuransi = data.getString("nominal_asuransi");
                                String referensi = data.getString("referensi");

                                stmt = mysqlq.getMysqlConn().createStatement();
                                mysqlq.setQuery("INSERT INTO T_SETORAN_ASURANSI (no_registrasi, bank_id,"
                                        + "no_referensi, " + "no_pasti_umrah, " + "kd_cabang, " + "nominal_asuransi, "
                                        + "tgl_bayar) VALUES ('" + noreg + "','" + bankid + "','" + referensi + "','"
                                        + nopasti + "','" + kdcabang + "','" + nom_asuransi + "','" + tglbayar + "')");
                                stmt.executeUpdate(mysqlq.getQuery());
                                log.info(mysqlq.getQuery());
                                stmt.close();

                            } else {
                                String info = obj.getString("message");
                                throw new UmrahSocketException(info);
                            }

                        } else {

                            throw new UmrahSocketException("Post bayar asuransi gagal");

                        }
                    } catch (Exception e) {

                        throw new UmrahSocketException(e.getMessage());

                    }

                }

                if (Request8.equals("Umroh Ganti Password")) {

                    // ----------------------- cek token first -----------------------------//
                    RequestToken reqToken = new RequestToken();
                    boolean cektoken = reqToken.cekExpiredToken();

                    if (cektoken) {
                        token = reqToken.getToken();
                    } else {
                        token = reqToken.getNewToken();
                        if (token.equals("")) {
                            log.error("Failed create token");
                            throw new Exception("Failed created token");
                        } else {
                            log.info("New token created");
                        }
                    }
                    // ----------------------------------------------------------------------//

                    ChangePassword cp = new ChangePassword();
                    cp.setNamaBank("BSM");
                    cp.setOldPassword(request.substring(187, 207).trim());
                    cp.setNewPassword(request.substring(207, 227).trim());
                    cp.setRetypePassword(request.substring(227, 247).trim());

                    ChangePasswordController req = new ChangePasswordController();
                    req.setCp(cp);

                    try {
                        req.doChangePassword();
                    } catch (Exception e) {
                        throw new UmrahSocketException(e.getMessage());
                    }

                    /* Send socket response */
                    LibFunction func = new LibFunction();
                    output_socket = func.str_pad("30001", 5, " ", "STR_PAD_RIGHT")
                            + func.str_pad("0", 8, "0", "STR_PAD_LEFT") + func.str_pad("0", 6, "0", "STR_PAD_LEFT")
                            + func.str_pad(req.getResult(), 50, " ", "STR_PAD_RIGHT");
                    log.info("OUTPUT SOCKET: " + output_socket);

                }

                if (Request9.equals("Umroh Inq Periode")) {

                    // ----------------------- cek token first -----------------------------//
                    RequestToken reqToken = new RequestToken();
                    boolean cektoken = reqToken.cekExpiredToken();

                    if (cektoken) {
                        token = reqToken.getToken();
                    } else {
                        token = reqToken.getNewToken();
                        if (token.equals("")) {
                            log.error("Failed create token");
                            throw new Exception("Failed created token");
                        } else {
                            log.info("New token created");
                        }
                    }
                    // ----------------------------------------------------------------------//

                    InquiryPeriode req = new InquiryPeriode();
                    req.setTglAkhir(request.substring(195, 199) + "-" + request.substring(199, 201) + "-"
                            + request.substring(201, 203));
                    req.setTglAwal(request.substring(187, 191) + "-" + request.substring(191, 193) + "-"
                            + request.substring(193, 195));
                    req.setPage("1");

                    InquiryPeriodeController cont = new InquiryPeriodeController();
                    cont.setInqPeriode(req);

                    try {

                        cont.doInqPeriode();

                    } catch (Exception e) {

                        throw new UmrahSocketException(e.getMessage());

                    }

                    /* Send socket response */
                    output_socket = cont.getResult();
                    log.info("OUTPUT SOCKET: " + output_socket);

                }

                if (Request10.equals("Umroh Inq Cabang")) {

                    // ----------------------- cek token first -----------------------------//
                    RequestToken reqToken = new RequestToken();
                    boolean cektoken = reqToken.cekExpiredToken();

                    if (cektoken) {
                        token = reqToken.getToken();
                    } else {
                        token = reqToken.getNewToken();
                        if (token.equals("")) {
                            log.error("Failed create token");
                            throw new Exception("Failed created token");
                        } else {
                            log.info("New token created");
                        }
                    }
                    // ----------------------------------------------------------------------//
                    
                    String cabang = "0" + request.substring(187, 190);
                    String tglawal = request.substring(190, 194) + "-" + request.substring(194, 196) + "-"
                            + request.substring(196, 198);
                    String tglakhir = request.substring(198, 202) + "-" + request.substring(202, 204) + "-"
                            + request.substring(204, 206);
                    String page = request.substring(206, 209);

                    InquiryKodeCabang param = new InquiryKodeCabang();
                    param.setKdCabang(cabang);
                    param.setTglAwal(tglawal);
                    param.setTglAkhir(tglakhir);
                    param.setPage(page);

                    InquiryKodeCabangController req = new InquiryKodeCabangController();
                    req.setInqCabang(param);

                    try {

                        req.doInqKodeCabang();

                    } catch (Exception e) {

                        throw new UmrahSocketException(e.getMessage());

                    }

                    /* Send socket response */
                    output_socket = req.getResult();
                    log.info("OUTPUT SOCKET: " + output_socket);

                }
                
                if (Request11.equals("Umroh Inq Debet")) {                    
                    
                    no_reg = request.substring(187, 191) + "-" + request.substring(191, 193) + "-" + request.substring(193, 195);
                    
                    InquiryTransaksiDebet param = new InquiryTransaksiDebet();
                    param.setNo_registrasi(no_reg);
                    
                    InquiryTransaksiDebetController req = new InquiryTransaksiDebetController();
                    req.setInquiryTransaksiDebet(param);
                    
                    try {
                        req.doInqTransaksiDebet();
                    } catch (Exception e) {
                        throw new UmrahSocketException(e.getMessage());
                    }
                    
                    /* Send socket response */
                    output_socket = req.getResult();
                    log.info("OUTPUT SOCKET: " + output_socket);
                    
                }
                
                if (Request12.equals("Inq Monitor per Registrasi") || Request12.equals("Inq Monitor per Ppiu")) {
                    
                    String flag = request.substring(187, 189);
                    String requestHeader = Request12;
                    String tglbayar = request.substring(189, 193) + "-" + request.substring(193, 195) + "-" + request.substring(195, 197);
                    
                    InquiryMonitor param = new InquiryMonitor();
                    param.setFlag(flag);
                    param.setRequestHeader(requestHeader);
                    param.setTglbayar(tglbayar);
                    
                    InquiryMonitorController req = new InquiryMonitorController();
                    req.setInqMonitor(param);
                    
                    try {
                        
                        req.doInqMonitor();
                        
                    } catch (Exception e) {
                        
                        throw new UmrahSocketException(e.getMessage());
                        
                    }
                    
                    /* Send socket response */
                    output_socket = req.getResult();
                    log.info("OUTPUT SOCKET: " + output_socket);
                    
                }
                
                /* send response */
                PrintWriter pr = new PrintWriter(socket.getOutputStream());
                pr.println(output_socket);
                pr.flush();

            } catch (Exception e) {

                log.error("Exception in SocketServer: " + e);
                LibFunction func = new LibFunction(); // gunakan function str pad untuk parsing response
                output_socket = func.str_pad("30001", 5, " ", "STR_PAD_RIGHT")
                        + func.str_pad("0", 8, "0", "STR_PAD_LEFT") // seq number
                        + func.str_pad("01", 6, "0", "STR_PAD_LEFT") // responsecode gagal 01
                        + func.str_pad(e.getMessage(), 100, " ", "STR_PAD_RIGHT");
                log.info("OUTPUT SOCKET : " + output_socket);

                /* send response server error */
                PrintWriter pr;
                try {
                    pr = new PrintWriter(socket.getOutputStream());
                    pr.println(output_socket);
                    pr.flush();
                } catch (IOException ex) {
                    log.error(ex.getMessage());
                }

            } finally {

                try {
                    socket.close();
                    log.info("Session closed");
                } catch (IOException e) {
                    log.error(e.getMessage());
                }

            }
        }

    }

}
