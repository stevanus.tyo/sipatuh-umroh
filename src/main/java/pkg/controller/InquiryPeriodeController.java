/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.log4j.Logger;
import org.json.me.JSONArray;
import org.json.me.JSONObject;

import pkg.lib.LibConfig;
import static pkg.main.SocketServer.token;
import pkg.model.InquiryPeriode;

/**
 *
 * @author macbook
 */
public class InquiryPeriodeController {

	private static Logger log = Logger.getLogger(InquiryPeriodeController.class);
	private InquiryPeriode inqPeriode;
	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public InquiryPeriode getInqPeriode() {
		return inqPeriode;
	}

	public void setInqPeriode(InquiryPeriode inqPeriode) {
		this.inqPeriode = inqPeriode;
	}

	public void doInqPeriode() {

		try {

			String tglAwal = inqPeriode.getTglAwal();
			String tglAkhir = inqPeriode.getTglAkhir();
			String page = inqPeriode.getPage();

			URL url = new URL(LibConfig.url_SOA + "transaksi" + "/" + tglAwal + "/" + tglAkhir + "/" + page);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("x-access-key", token);
			conn.setConnectTimeout(6000);

			log.info("Sending 'GET' request to URL : " + url);
			log.info("Response Code : " + conn.getResponseCode());

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed HTTP Error code " + conn.getResponseCode());
			}

			InputStreamReader is = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(is);
			String output;

			while ((output = br.readLine()) != null) {

				log.info("RESPONSE REST : " + output);

				JSONObject obj = new JSONObject(output);
				String rc = obj.getString("RC");
				String message = obj.getString("message");
				JSONArray data = obj.getJSONArray("data");
				JSONArray arr = new JSONArray();

				for (int i = 0; i < data.length(); i++) {

					String noreg = data.getJSONObject(i).getString("nomor_registrasi");
					String nopasti = data.getJSONObject(i).getString("nomor_pasti_umrah");
					String bankid = data.getJSONObject(i).getString("bank_id");
					String nama = data.getJSONObject(i).getString("nama");
					String ppiu = data.getJSONObject(i).getString("ppiu");
					String asuransi = data.getJSONObject(i).getString("asuransi");
					String paket = data.getJSONObject(i).getString("paket");
					String nomass = data.getJSONObject(i).getString("nominal_asuransi");
					String nomppiu = data.getJSONObject(i).getString("nominal_ppiu");
					String nomtot = data.getJSONObject(i).getString("nominal_total");
					String kdcabang = data.getJSONObject(i).getString("kd_cabang");
					String noref = data.getJSONObject(i).getString("nomor_referensi");
					String tglbyr = data.getJSONObject(i).getString("tgl_bayar");

					JSONArray arr2 = new JSONArray();
					arr2.put(noreg);
					arr2.put(nopasti);
					arr2.put(bankid);
					arr2.put(nama);
					arr2.put(ppiu);
					arr2.put(asuransi);
					arr2.put(paket);
					arr2.put(nomass);
					arr2.put(nomppiu);
					arr2.put(nomtot);
					arr2.put(kdcabang);
					arr2.put(noref);
					arr2.put(tglbyr);
					arr.put(arr2);

				}

				JSONObject resobj = new JSONObject();
				resobj.put("RC", rc);
				resobj.put("message", message);
				resobj.put("data", arr);

				result = resobj.toString();
			}

		} catch (Exception e) {

			result = e.getMessage();
			log.error(e.getMessage());

		}

	}

}
