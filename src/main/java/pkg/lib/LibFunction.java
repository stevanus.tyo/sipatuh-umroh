/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.lib;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 *
 * @author macbook
 */
public class LibFunction {

    private Connection mysql_db_conn;
    private boolean reconnect = true;
    private boolean result = false;
    private String err_message = "";
    private String query = "";
    private String hasil;

    static Logger log = Logger.getLogger(LibFunction.class);

    public static String GetCurrentDirectory() {
        String result = "";
        File dir = null;
        try {
            dir = new File(".");
            result = dir.getCanonicalPath();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return result = "";
        }
        return result.trim();
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return this.query;
    }

    public String str_pad(String input, int length, String pad, String sense) {
        int resto_pad = length - input.length();
        String padded = "";

        if (resto_pad <= 0) {
            return input;
        }

        if (sense.equals("STR_PAD_RIGHT")) {
            padded = input;
            padded += _fill_string(pad, resto_pad);
        } else if (sense.equals("STR_PAD_LEFT")) {
            padded = _fill_string(pad, resto_pad);
            padded += input;
        } else // STR_PAD_BOTH
        {
            int pad_left = (int) Math.ceil(resto_pad / 2);
            int pad_right = resto_pad - pad_left;

            padded = _fill_string(pad, pad_left);
            padded += input;
            padded += _fill_string(pad, pad_right);
        }
        return padded;
    }

    protected String _fill_string(String pad, int resto) {
        boolean first = true;
        String padded = "";

        if (resto >= pad.length()) {
            for (int i = resto; i >= 0; i = i - pad.length()) {
                if (i >= pad.length()) {
                    if (first) {
                        padded = pad;
                    } else {
                        padded += pad;
                    }
                } else {
                    if (first) {
                        padded = pad.substring(0, i);
                    } else {
                        padded += pad.substring(0, i);
                    }
                }
                first = false;
            }
        } else {
            padded = pad.substring(0, resto);
        }
        return padded;
    }

    public static String getDatetime(String format) {
        DateFormat dateFormat = null;
        Date date = null;
        try {
            dateFormat = new SimpleDateFormat(format.trim());
            date = new Date();
        } catch (Exception ex) {
            return "";
        }
        return dateFormat.format(date);
    }

    public static String getAddDate(String format, int add) {

        DateFormat dateFormat = new SimpleDateFormat(format);
        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, add);
        Date currentAddDate = c.getTime();

        return dateFormat.format(currentAddDate);
    }

    public static String setTimezone(String formatdatetime, int timezone, String datetime) {

        int year = Integer.parseInt(datetime.substring(0, 4));
        int month = Integer.parseInt(datetime.substring(6, 7)) - 1;
        int day = Integer.parseInt(datetime.substring(8, 10));
        int hour = Integer.parseInt(datetime.substring(11, 13));
        int minute = Integer.parseInt(datetime.substring(14, 16));
        int second = Integer.parseInt(datetime.substring(17, 19));

        DateFormat dateFormat = new SimpleDateFormat(formatdatetime);
        Calendar c = Calendar.getInstance();
        c.set(year, month, day, hour, minute, second);
        c.add(c.HOUR, timezone);
        Date currentAddDate = c.getTime();

        return dateFormat.format(currentAddDate);
    }

    public Connection getMysqlConn() {
        if (this.mysql_db_conn == null) {
            try {
                DriverManager.setLoginTimeout(10000);
                this.mysql_db_conn = DriverManager.getConnection("jdbc:mysql://" + LibConfig.db_host + ":"
                        + LibConfig.db_port + "/" + LibConfig.db_name + "?autoReconnect=true&useUnicode=yes",
                        LibConfig.db_user, LibConfig.db_pass);
                setResult(true);
                setReconnect(true);
            } catch (SQLException Ex) {
                closeMysqlDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
                log.error(Ex.getMessage());
            } catch (Exception Ex) {
                closeMysqlDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
                log.error(Ex.getMessage());
            }
        }

        return this.mysql_db_conn;
    }

    private void setResult(boolean result) {
        this.result = result;
    }

    public void setReconnect(boolean reconnect) {
        this.reconnect = reconnect;
    }

    public void closeMysqlDBConn() {
        if (this.mysql_db_conn != null) {
            try {
                this.mysql_db_conn.close();
            } catch (SQLException Ex) {
            } catch (Exception Ex) {
            }
        }
    }

    public void setErrMessage(String err_message) {
        try {
            err_message = err_message.trim();
            if (!(err_message == null || err_message.isEmpty())) {
                this.err_message = err_message;
            }
        } catch (Exception Ex) {
            this.err_message = Ex.getMessage();
        }
    }

    public int getTotalSetoran(String no_registrasi) {

        Statement stmt;
        ResultSet getResultSet;
        int total_setoran = 0;

        try {

            stmt = getMysqlConn().createStatement();
            setQuery("SELECT SUM(DEBIT_AMT) AS TOTAL_SETORAN FROM T_T24_OVERBOOK " + "WHERE STATUS = 'Success' "
                    + "AND NO_REGISTRASI = '" + no_registrasi + "' " + "AND KETERANGAN NOT LIKE '%asuransi%';");
            getResultSet = stmt.executeQuery(getQuery());

            boolean cekfetchall = getResultSet.next();
            if (!cekfetchall) {
                throw new UmrahSocketException("Error getting total setoran");
            } else {
                total_setoran = getResultSet.getInt("TOTAL_SETORAN");
            }
            stmt.close();

        } catch (Exception e) {

            log.error(e.getMessage());

        }

        return total_setoran;

    }

    public boolean cekSetoranAwal(String no_registrasi) {

        Statement stmt;
        ResultSet getResultSet;

        try {

            stmt = getMysqlConn().createStatement();
            setQuery("SELECT * FROM t_flag_setoran_awal WHERE no_registrasi = '" + no_registrasi + "'");
            getResultSet = stmt.executeQuery(getQuery());

            boolean cekfetchall = getResultSet.next();

            if (!cekfetchall) {
                stmt.close();
                return false;
            } else {
                stmt.close();
                return true;
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    public boolean cekSetoranKetiga(String no_registrasi) {

        Statement stmt;
        ResultSet getResultSet;

        try {
            stmt = getMysqlConn().createStatement();
            setQuery("SELECT COUNT(no_registrasi) as jumlah from T_SETORAN WHERE no_registrasi = '" + no_registrasi + "'");
            getResultSet = stmt.executeQuery(getQuery());
            while (getResultSet.next()) {
                hasil = getResultSet.getString("jumlah");
            }
            if (hasil.equals("2")) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

}
