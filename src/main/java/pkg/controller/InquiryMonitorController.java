/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;
import org.json.me.JSONArray;
import org.json.me.JSONObject;
import pkg.lib.LibFunction;
import pkg.model.InquiryMonitor;

/**
 *
 * @author macbook
 */
public class InquiryMonitorController {

    private static Logger log = Logger.getLogger(InquiryMonitorController.class);

    private String result;
    private InquiryMonitor inqMonitor;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public InquiryMonitor getInqMonitor() {
        return inqMonitor;
    }

    public void setInqMonitor(InquiryMonitor inqMonitor) {
        this.inqMonitor = inqMonitor;
    }

    public String doInqMonitor() {

        String requestHeader = inqMonitor.getRequestHeader();
        String flag = inqMonitor.getFlag();
        String tglbayar = inqMonitor.getTglbayar();
        Statement stmt;
        ResultSet getResultSet;

        JSONArray array = new JSONArray();

        if (requestHeader.equals("Inq Monitor per Registrasi")) {
            try {

                if (flag.equals("01")) {
                    flag = "<";
                } else {
                    flag = ">=";
                }

                stmt = new LibFunction().getMysqlConn().createStatement();
                getResultSet = stmt.executeQuery("SELECT "
                        + "	a.no_registrasi AS NOMOR_REGISTRASI,"
                        + "	a.no_pasti_umrah AS NO_PASTI,"
                        + "	a.akumulasi AS AKUMULASI,"
                        + "	b.ID_PPIU,"
                        + "	b.NAMA_PPIU "
                        + "FROM "
                        + "	("
                        + "	SELECT"
                        + "		c.no_registrasi,"
                        + "		c.no_pasti_umrah,"
                        + "		sum( c.nominal_total ) AS akumulasi "
                        + "	FROM "
                        + "		( SELECT no_registrasi, no_pasti_umrah, nominal_total, tgl_bayar FROM T_SETORAN WHERE tgl_bayar LIKE '" + tglbayar + "%' ) c "
                        + "	GROUP BY "
                        + "		c.no_registrasi "
                        + "	) a "
                        + "	LEFT JOIN ( SELECT ID_PPIU, NAMA_PPIU FROM T_PPIU ) b ON SUBSTR( a.no_registrasi, 1, 4 ) = b.ID_PPIU "
                        + "WHERE "
                        + "	a.akumulasi " + flag + " 15000000");

                while (getResultSet.next()) {

                    String noreg = getResultSet.getString("NOMOR_REGISTRASI");
                    String nopasti = getResultSet.getString("NO_PASTI");
                    String akumulasi = getResultSet.getString("AKUMULASI");
                    String idppiu = getResultSet.getString("ID_PPIU");
                    String namappiu = getResultSet.getString("NAMA_PPIU");

                    JSONArray arr = new JSONArray();

                    arr.put(noreg);
                    arr.put(nopasti);
                    arr.put(akumulasi);
                    arr.put(idppiu);
                    arr.put(namappiu);

                    array.put(arr);

                }

                if (array.length() > 0) {

                    JSONObject resobj = new JSONObject();
                    resobj.put("RC", "00");
                    resobj.put("message", "Berhasil");
                    resobj.put("data", array);

                    result = resobj.toString();

                } else {

                    JSONObject resobj = new JSONObject();
                    resobj.put("RC", "01");
                    resobj.put("message", "Data tidak ditemukan");
                    resobj.put("data", array);
                    throw new Exception(resobj.toString());

                }

            } catch (Exception e) {
                result = e.getMessage();
                log.error(e.getMessage());
            }

        } else {

            try {

                if (flag.equals("01")) {
                    flag = "<";
                } else {
                    flag = ">=";
                }

                stmt = new LibFunction().getMysqlConn().createStatement();
                getResultSet = stmt.executeQuery("SELECT "
                        + "	b.ID_PPIU,"
                        + "	b.NAMA_PPIU,"
                        + "	count( b.NAMA_PPIU ) AS TOTAL,"
                        + "	sum( c.akumulasi ) AS AKUMULASI "
                        + "FROM "
                        + "	("
                        + "	SELECT "
                        + "		a.no_registrasi,"
                        + "		a.no_pasti_umrah,"
                        + "		a.akumulasi "
                        + "	FROM "
                        + "		("
                        + "		SELECT "
                        + "			c.no_registrasi,"
                        + "			c.no_pasti_umrah,"
                        + "			sum( c.nominal_total ) AS akumulasi "
                        + "		FROM "
                        + "			( SELECT no_registrasi, no_pasti_umrah, nominal_total, tgl_bayar FROM T_SETORAN WHERE tgl_bayar LIKE '" + tglbayar + "%' ) c "
                        + "		GROUP BY "
                        + "			c.no_registrasi "
                        + "		) a "
                        + "	WHERE "
                        + "		a.akumulasi " + flag + " 15000000 "
                        + "	) c"
                        + "	LEFT JOIN ( SELECT ID_PPIU, NAMA_PPIU FROM T_PPIU ) b ON SUBSTR( c.no_registrasi, 1, 4 ) = b.ID_PPIU "
                        + "GROUP BY "
                        + "	b.ID_PPIU, "
                        + "	b.NAMA_PPIU");

                while (getResultSet.next()) {

                    String total = getResultSet.getString("TOTAL");
                    String akumulasi = getResultSet.getString("AKUMULASI");
                    String idppiu = getResultSet.getString("ID_PPIU");
                    String namappiu = getResultSet.getString("NAMA_PPIU");

                    JSONArray arr = new JSONArray();

                    arr.put(idppiu);
                    arr.put(namappiu);
                    arr.put(total);
                    arr.put(akumulasi);

                    array.put(arr);

                }

                if (array.length() > 0) {

                    JSONObject resobj = new JSONObject();
                    resobj.put("RC", "00");
                    resobj.put("message", "Berhasil");
                    resobj.put("data", array);

                    result = resobj.toString();

                } else {

                    JSONObject resobj = new JSONObject();
                    resobj.put("RC", "01");
                    resobj.put("message", "Data tidak ditemukan");
                    resobj.put("data", array);
                    throw new Exception(resobj.toString());

                }

            } catch (Exception e) {

                result = e.getMessage();
                log.error(e.getMessage());

            }

        }

        return result;
    }

}
