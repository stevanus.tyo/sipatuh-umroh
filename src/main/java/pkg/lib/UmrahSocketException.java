/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.lib;

/**
 *
 * @author macbook
 */
public class UmrahSocketException extends Exception {
	private String error;

	public UmrahSocketException(String msg) {
		super(msg);
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
