/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.model;

/**
 *
 * @author macbook
 */
public class InquiryMonitor {
    
    private String flag;
    private String requestHeader;
    private String tglbayar;

    public String getTglbayar() {
        return tglbayar;
    }

    public void setTglbayar(String tglbayar) {
        this.tglbayar = tglbayar;
    }

    public String getRequestHeader() {
        return requestHeader;
    }

    public void setRequestHeader(String requestHeader) {
        this.requestHeader = requestHeader;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
    
}
