/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.lib;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.json.me.JSONObject;

/**
 *
 * @author macbook
 */
public class RequestToken {

	static Logger log = Logger.getLogger(RequestToken.class);

	public String token;

	public boolean cekExpiredToken() throws ParseException {

		LibFunction mysqlq = new LibFunction();
		Statement stmt;
		ResultSet getResultSet;
		Date expdate = null;
		String datenow = LibFunction.getDatetime("yyyy-MM-dd");
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(datenow);

		try {
			stmt = mysqlq.getMysqlConn().createStatement();
			mysqlq.setQuery("SELECT TOKEN, EXPIRED_DATE FROM REQUEST_TOKEN;");
			getResultSet = stmt.executeQuery(mysqlq.getQuery());
			while (getResultSet.next()) {
				token = getResultSet.getString("TOKEN");
				expdate = getResultSet.getDate("EXPIRED_DATE");
			}
			if (expdate.compareTo(date) > 0) {
				log.info("Token is valid");
				return true;
			} else {
				log.info("Token invalid, get new token");
				return false;
			}
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}

	public String getNewToken() throws IOException {

		LibFunction mysqlq = new LibFunction();
		Statement stmt;
		String dateadd = LibFunction.getAddDate("yyyy-MM-dd", 5);

		try {
			String apikey = LibConfig.api_key;

			JSONObject requestbody = new JSONObject();
			requestbody.put("userid", LibConfig.api_username);
			requestbody.put("password", LibConfig.api_password);

			if (LibConfig.useProxy.equals("true")) {
				System.setProperty("java.net.useSystemProxies", LibConfig.useProxy);
				System.setProperty("http.proxyHost", "172.20.0.5");
				System.setProperty("http.proxyPort", "8080");
			} else {
				System.clearProperty("http.proxyHost");
			}

			URL url = new URL(LibConfig.url_SOA + "login");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("x-key", apikey);

			String urlParameters = requestbody.toString();

			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = conn.getResponseCode();

			System.clearProperty("http.proxyHost");

			if (responseCode == 200) {
				log.info("Sending 'POST' request to URL : " + url);
				log.info("Post parameters : " + urlParameters);
				log.info("Response Code : " + responseCode);

				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = br.readLine()) != null) {
					response.append(inputLine);
				}
				br.close();

				// log result
				log.info("RESPONSE REST : " + response.toString());
				String output = response.toString();

				// json response parse new token
				JSONObject obj = new JSONObject(output);
				token = obj.getString("token");

				// update new token into db
				stmt = mysqlq.getMysqlConn().createStatement();
				mysqlq.setQuery(
						"UPDATE REQUEST_TOKEN " + "SET TOKEN = '" + token + "'," + "EXPIRED_DATE = '" + dateadd + "'");
				stmt.executeUpdate(mysqlq.getQuery());
			} else {
				throw new Exception("Request Token Gagal !");
			}
		} catch (Exception e) {
			log.error(e);
			token = "";
		}
		return token;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
