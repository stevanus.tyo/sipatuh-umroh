/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.lib;

import org.apache.log4j.Logger;

/**
 *
 * @author macbook
 */
public class InitConnection {

	private static Logger log = Logger.getLogger(InitConnection.class);

	public boolean initializeProxy;

	public boolean setProxyConnection() {

		String status;

		if (LibConfig.useProxy.equals("true")) {

			System.setProperty("java.net.useSystemProxies", LibConfig.useProxy);
			System.setProperty("http.proxyHost", LibConfig.hostProxy);
			System.setProperty("http.proxyPort", LibConfig.portProxy);
			log.info("System connection initialize with proxy....");
			return true;

		} else {

			log.info("No proxy connection is configured....");
			return false;
		}

	}

	public boolean getInitializeProxy() {

		return initializeProxy = this.setProxyConnection();

	}

}
