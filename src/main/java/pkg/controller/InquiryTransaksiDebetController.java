/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.controller;

import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;
import org.json.me.JSONArray;
import org.json.me.JSONObject;
import pkg.lib.LibFunction;
import pkg.model.InquiryTransaksiDebet;

/**
 *
 * @author macbook
 */
public class InquiryTransaksiDebetController {

    private static Logger log = Logger.getLogger(InquiryTransaksiDebetController.class);
    private InquiryTransaksiDebet inquiryTransaksiDebet;
    private String result;

    public InquiryTransaksiDebet getInquiryTransaksiDebet() {
        return inquiryTransaksiDebet;
    }

    public void setInquiryTransaksiDebet(InquiryTransaksiDebet inquiryTransaksiDebet) {
        this.inquiryTransaksiDebet = inquiryTransaksiDebet;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void doInqTransaksiDebet() {

        Statement stmt;
        ResultSet getResultSet;

        JSONArray array = new JSONArray();

        try {
            String no_reg = inquiryTransaksiDebet.getNo_registrasi();
            stmt = new LibFunction().getMysqlConn().createStatement();
            getResultSet = stmt.executeQuery("SELECT * from T_T24_OVERBOOK WHERE DATETIME LIKE '" + no_reg + "%'");

            while (getResultSet.next()) {
                String nomor_reg = getResultSet.getString("NO_REGISTRASI");
                String debitacc = getResultSet.getString("DEBIT_ACC");
                String debitamt = getResultSet.getString("DEBIT_AMT");
                String creditacc = getResultSet.getString("CREDIT_ACC");
                String msgid = getResultSet.getString("MSG_ID");
                String status = getResultSet.getString("STATUS");
                String ft = getResultSet.getString("FT");
                String datetime = getResultSet.getString("DATETIME");
                String keterangan = getResultSet.getString("KETERANGAN");

                JSONArray arr = new JSONArray();
                arr.put(nomor_reg);
                arr.put(debitacc);
                arr.put(debitamt);
                arr.put(creditacc);
                arr.put(msgid);
                arr.put(status);
                arr.put(ft);
                arr.put(datetime);
                arr.put(keterangan);

                array.put(arr);
            }

            if (array.length() > 0) {

                JSONObject resobj = new JSONObject();
                resobj.put("RC", "00");
                resobj.put("message", "Berhasil");
                resobj.put("data", array);

                result = resobj.toString();

            } else {
                
                JSONObject resobj = new JSONObject();
                resobj.put("RC", "01");
                resobj.put("message", "Nomor registrasi tidak ditemukan");
                resobj.put("data", array);
                throw new Exception(resobj.toString());
                
            }

        } catch (Exception e) {

            result = e.getMessage();
            log.error(e.getMessage());

        }

    }

}
