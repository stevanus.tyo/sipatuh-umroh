/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.lib;

/**
 *
 * @author macbook
 */
public class LibConfig {

	public static String db_connection_properties = "autoReconnect=true" + "&connectionCollation=latin1_general_ci"
			+ "&useGmtMillisForDatetimes=false";
	public static String file_config_xml = LibFunction.GetCurrentDirectory() + "/appconfig.xml";

	public static String db_host = XMLParser.XMLConfig("config", "dbHost");
	public static String db_port = XMLParser.XMLConfig("config", "dbPort");
	public static String db_user = XMLParser.XMLConfig("config", "dbUsername");
	public static String db_pass = XMLParser.XMLConfig("config", "dbPassword");
	public static String db_name = XMLParser.XMLConfig("config", "dbName");
	public static String db_timeout = XMLParser.XMLConfig("config", "dbTimeout").toString();

	public static String time_execute = XMLParser.XMLConfig("config", "timeExe");

	public static String url_end_point = XMLParser.XMLConfig("config", "urlEndPoint");
	public static String username_TWS = XMLParser.XMLConfig("config", "usernameTWS");
	public static String password_TWS = XMLParser.XMLConfig("config", "passwordTWS");
	public static String company_TWS = XMLParser.XMLConfig("config", "companyTWS");
	public static String biaya = XMLParser.XMLConfig("config", "biaya");
	public static String jnsbiaya = XMLParser.XMLConfig("config", "jnsbiaya");

	public static String url_SOA = XMLParser.XMLConfig("config", "urlSOA");
	public static String api_username = XMLParser.XMLConfig("config", "apiUsername");
	public static String api_password = XMLParser.XMLConfig("config", "apiPassword");
	public static String api_key = XMLParser.XMLConfig("config", "apiKey");

	public static String stringPort = XMLParser.XMLConfig("config", "port");

	public static int port = Integer.parseInt(stringPort);

	public static String useProxy = XMLParser.XMLConfig("config", "useProxy");
	protected static String hostProxy = XMLParser.XMLConfig("config", "proxyHost");
	protected static String portProxy = XMLParser.XMLConfig("config", "proxyPort");
        public static String stringTimezone = XMLParser.XMLConfig("config", "Timezone");
        
        public static int timezone = Integer.parseInt(stringTimezone);

}
