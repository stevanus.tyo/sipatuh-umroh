/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.lib;

import java.io.ByteArrayOutputStream;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.apache.log4j.Logger;
import pkg.main.SocketServer;

/**
 *
 * @author macbook
 */
public class FundTransfer {

	private static Logger log = Logger.getLogger(FundTransfer.class);

	public static void createSoapEnvelope(SOAPMessage soapMessage) throws SOAPException {
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String myNamespace = "t24";
		String myNamespaceURI = "T24WebServicesImpl";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();

		SOAPElement soapBodyElem = soapBody.addChildElement("FundsTransfer", myNamespace);

		SOAPElement soapBodyElemWebReqCom = soapBodyElem.addChildElement("WebRequestCommon");
		SOAPElement soapBodyElemargWebReqCom1 = soapBodyElemWebReqCom.addChildElement("userName");
		SOAPElement soapBodyElemargWebReqCom2 = soapBodyElemWebReqCom.addChildElement("password");
		SOAPElement soapBodyElemargWebReqCom3 = soapBodyElemWebReqCom.addChildElement("company");
		SOAPElement soapBodyElemOFS = soapBodyElem.addChildElement("OfsFunction");
		SOAPElement soapBodyElemargOFS1 = soapBodyElemOFS.addChildElement("messageId");
		SOAPElement soapBodyElemFUNDSTRANSFERID = soapBodyElem.addChildElement("FUNDSTRANSFERIDIACCTTRFCMSType");
		SOAPElement soapBodyElemargFUNDSTRANSFERID1 = soapBodyElemFUNDSTRANSFERID.addChildElement("DebitAccount");
		SOAPElement soapBodyElemargFUNDSTRANSFERID2 = soapBodyElemFUNDSTRANSFERID.addChildElement("DebitAmount");
		SOAPElement soapBodyElemargFUNDSTRANSFERID3 = soapBodyElemFUNDSTRANSFERID.addChildElement("CreditAccount");
		SOAPElement soapBodyElemargFUNDSTRANSFERID4sub = soapBodyElemFUNDSTRANSFERID.addChildElement("gPAYMENTDETAILS");
		SOAPElement soapBodyElemargFUNDSTRANSFERID4sub1 = soapBodyElemargFUNDSTRANSFERID4sub
				.addChildElement("PaymentDetails");
		SOAPElement soapBodyElemargFUNDSTRANSFERID5 = soapBodyElemFUNDSTRANSFERID.addChildElement("KodeBiaya");
		SOAPElement soapBodyElemargFUNDSTRANSFERID6sub = soapBodyElemFUNDSTRANSFERID.addChildElement("gCOMMISSIONTYPE");
		SOAPElement soapBodyElemargFUNDSTRANSFERID6sub1 = soapBodyElemargFUNDSTRANSFERID6sub
				.addChildElement("mCOMMISSIONTYPE");
		SOAPElement soapBodyElemargFUNDSTRANSFERID6sub1ch1 = soapBodyElemargFUNDSTRANSFERID6sub1
				.addChildElement("JenisBiaya");
		SOAPElement soapBodyElemargFUNDSTRANSFERID6sub1ch2 = soapBodyElemargFUNDSTRANSFERID6sub1
				.addChildElement("NominalBiaya");
		SOAPElement soapBodyElemargFUNDSTRANSFERID7 = soapBodyElemFUNDSTRANSFERID.addChildElement("MSGID");

		/* Setting Parameter to SOAP BODY */
		SocketServer isi = new SocketServer();

		soapBodyElemargWebReqCom1.addTextNode(LibConfig.username_TWS); // username
		soapBodyElemargWebReqCom2.addTextNode(LibConfig.password_TWS); // password
		soapBodyElemargWebReqCom3.addTextNode("ID001" + isi.kd_cabang); // company
		soapBodyElemargOFS1.addTextNode(isi.msgid); // msgid
		soapBodyElemargFUNDSTRANSFERID1.addTextNode(isi.no_rek_dbt); // accno
		soapBodyElemargFUNDSTRANSFERID2.addTextNode(isi.nmlppiu); // debitamt
		soapBodyElemargFUNDSTRANSFERID3.addTextNode(isi.no_rek_ppiu); // creditaccno
		soapBodyElemargFUNDSTRANSFERID4sub1.addTextNode("setoran umroh noreg " + isi.no_reg); // paymentdtl
		// soapBodyElemargFUNDSTRANSFERID5.addTextNode("6010"); //kodebiaya
		soapBodyElemargFUNDSTRANSFERID6sub1ch1.addTextNode(LibConfig.jnsbiaya); // jenisbiaya
		soapBodyElemargFUNDSTRANSFERID6sub1ch2.addTextNode("IDR" + LibConfig.biaya); // nominalbiaya
		soapBodyElemargFUNDSTRANSFERID7.addTextNode(isi.msgid); // msgid

	}

	public SOAPMessage callSoapWebService(String soapEndpointUrl, String soapAction) {
		SOAPMessage soapResponse = null;
		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			// Send SOAP Message to SOAP Server
			soapResponse = soapConnection.call(createSOAPRequest(soapAction), soapEndpointUrl);

			// Print the SOAP Response
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			soapResponse.writeTo(out);
			String soapmessage = new String(out.toByteArray());
			log.info("Response SOAP Message:" + soapmessage);
			System.out.println("\n");

			soapConnection.close();

		} catch (Exception e) {
			System.err.println(
					"\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
			e.printStackTrace();
		}

		return soapResponse;
	}

	public static SOAPMessage createSOAPRequest(String soapAction) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();

		createSoapEnvelope(soapMessage);

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", soapAction);

		soapMessage.saveChanges();

		/* Print the request message, just for debugging purposes */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		soapMessage.writeTo(out);
		String soaprequest = new String(out.toByteArray());
		log.info("Request SOAP Message:" + soaprequest);
		System.out.println("\n");

		return soapMessage;
	}

}
