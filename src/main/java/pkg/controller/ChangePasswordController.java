/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.log4j.Logger;
import org.json.me.JSONObject;

import pkg.lib.LibConfig;
import pkg.lib.UmrahSocketException;
import static pkg.main.SocketServer.token;
import pkg.model.ChangePassword;

/**
 *
 * @author macbook
 */
public class ChangePasswordController {

	private static Logger log = Logger.getLogger(ChangePasswordController.class);

	private ChangePassword cp;

	private String result;

	public static Logger getLog() {
		return log;
	}

	public static void setLog(Logger log) {
		ChangePasswordController.log = log;
	}

	public ChangePassword getCp() {
		return cp;
	}

	public void setCp(ChangePassword cp) {
		this.cp = cp;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public void doChangePassword() {
		try {

			JSONObject reqBody = new JSONObject();
			reqBody.put("nama", cp.getNamaBank());
			reqBody.put("password", cp.getOldPassword());
			reqBody.put("password_baru", cp.getNewPassword());
			reqBody.put("password_baru_check", cp.getRetypePassword());

			URL url = new URL(LibConfig.url_SOA + "password");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("x-access-key", token);
			conn.setConnectTimeout(6000);

			String urlParameters = reqBody.toString();

			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = conn.getResponseCode();

			log.info("Sending 'POST' request to URL : " + url);
			log.info("Post parameters : " + urlParameters);
			log.info("Response Code : " + responseCode);

			if (responseCode == 200) {

				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = br.readLine()) != null) {
					response.append(inputLine);
				}
				br.close();

				/* print log response rest */
				log.info("RESPONSE REST : " + response.toString());

				/* response for socket */
				result = "Ganti Password Berhasil";

			} else {

				/* response for socket */
				result = "Ganti Password Gagal";
				throw new UmrahSocketException("Ganti Password Gagal");
			}

		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

}
